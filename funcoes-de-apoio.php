<?php

function ordenaMeses($listaCompras){
    $meses = [
        'janeiro' => [],
        'fevereiro' => [],
        'marco' => [],
        'abril' => [],
        'maio' => [],
        'junho' => [],
    ];

    foreach ($listaCompras as $listaMes => $listaCategorias) {
        foreach ($meses as $mes => $value) {
            if($listaMes == $mes){
                $categoriasOrdenadas = ordenaCategorias($listaCompras[$listaMes]);
                $meses[$mes] = $categoriasOrdenadas;
            }
        }
    }

    $meses = array_filter($meses, 'array_filter');
    return $meses;
}

function ordenaCategorias($listaCategorias){
    $novaOrdem = [
        'alimentos' => [],
        'higiene_pessoal' => [],
        'limpeza' => []
    ];

    foreach ($listaCategorias as $categoria => $listaItens) {
        foreach ($listaItens as $item => $v1) {
            if($item == 'Papel Hignico'){
                $listaItens['Papel Higiênico'] = $v1;
                unset($listaItens[$item]);
            }
            if($item == 'Brocolis'){
                $listaItens['Brócolis'] = $v1;
                unset($listaItens[$item]);
            }
            if($item == 'Chocolate ao leit'){
                $listaItens['Chocolate ao leite'] = $v1;
                unset($listaItens[$item]);
            }
            if($item == 'Sabao em po'){
                $listaItens['Sabão em pó'] = $v1;
                unset($listaItens[$item]);
            }
            if($item == 'Geléria de morango'){
                $listaItens['Geléia de morango'] = $v1;
                unset($listaItens[$item]);
            }
        }
        foreach ($novaOrdem as $cat => $v2) {
            if($categoria == $cat){
                arsort($listaItens);
                $novaOrdem[$cat] = $listaItens;
            }
        }
    }

    return $novaOrdem;
}

function geraCSV($listaOrdenada){
    $line = 0;
    $csv[$line] = array('Mes', 'Categoria', 'Produto', 'Quantidade');

    foreach ($listaOrdenada as $listaMes => $categorias) {
        foreach ($categorias as $categoria => $itens) {
            foreach ($itens as $item => $value) {
                $line++;
                $csv[$line] = [$listaMes, $categoria, $item, $value];
            }
        }
    }

    $fp = fopen('compras-do-ano.csv', 'w');

    foreach ($csv as $fields) {
        fputcsv($fp, $fields, ';');
    }

    fclose($fp);
}